const express = require('express');
const router = express.Router();
const devices = require('../bl/devices');

router.get('/', devices.getAllDevices);
router.get('/:id', devices.getDeviceById);
router.post('/', devices.createDevice);
router.put('/', devices.updateDevice);
router.delete('/:id', devices.deleteDevice);

module.exports = router;