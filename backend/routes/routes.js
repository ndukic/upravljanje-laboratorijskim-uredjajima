const express = require('express');
const router = express.Router();

const auth = require('./auth');
const users = require('./users');
const devices = require('./devices');
const commands = require('./commands');

/* API ping */
router.get('/', function(req, res) {
    res.status(200).send('Upravljanje laboratorijskim uredjajima API');
});

/* Add layer of authentication protection */
// router.post('/authenticate', auth.authenticate);
// router.use(auth.verify);

router.use('/users', users);
router.use('/devices', devices);
router.use('/commands', commands);

module.exports = router;