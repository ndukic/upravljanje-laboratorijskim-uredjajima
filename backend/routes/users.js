const express = require('express');
const router = express.Router();
const users = require('../bl/users');

router.get('/', users.getAllUsers);
//app.get('/users/:id', users.getUserById);     // TODO
router.post('/', users.createUser);
//app.put('/users', users.updateDevice);        // TODO
//app.delete('/users/:id', users.deleteUser);   // TODO
router.get('/currentUser', users.getCurrentUser);  // get logged in user

module.exports = router;