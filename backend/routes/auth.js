const config = require('../config');
const dal = require('../dal/dal');
const jwt = require('jsonwebtoken');

function authenticate(req, res) {
    console.log(req.body.username);
    if (!req.body.username || !req.body.password) {
        return res.status(404).send({
            success: false,
            message: "Username or password is missing"
        });
    }
    dal.getUserByUsernameDAL(req.body.username, function(err, user) {
        if (err) {
            console.log('Error in authentication');
            res.status(403).send({
                success: false,
                message: 'Authentication failed: username does not exist'
            });
        } else {
            if (user && user.password && user.password === req.body.password) {
                var payload = {
                    id: user.id
                }
                var options = {
                    expiresIn: 60*60*24 // Expires in 24h - TODO export to config
                };
                var token = jwt.sign(payload, config.secret, options);
                res.status(200).send({
                    success: true, 
                    token: token
                })
            } else {
                res.status(403).send({
                    success: false,
                    message: 'Authentication failed: wrong password'
                });
            }
        }
    });
}

function verify(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if (token) {
        jwt.verify(req.body.token, config.secret, function(err, decoded) {
            if (err) {
                if (err.name === 'TokenExpiredError') {
                    return res.status(403).send('Authorization failed: token expired');    
                }
                return res.status(403).send('Authorization failed: bad token');
            }
            req.decoded = decoded;
            next();
        });
    } else {
        return res.status(403).send({ 
            success: false, 
            message: 'No token provided.' 
        });
    }
}

module.exports = {
    authenticate,
    verify
};