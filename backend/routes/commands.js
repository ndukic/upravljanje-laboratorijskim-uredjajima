const express = require('express');
const router = express.Router();
const commands = require('../bl/commands');

router.get('/', commands.getAllCommands);      
router.get('/:id', commands.getCommandById);  
router.post('/', commands.createCommand);      
router.put('/', commands.updateCommand);       
router.delete('/:id', commands.deleteCommand);
router.post('/execute', commands.executeCommand);

module.exports = router;