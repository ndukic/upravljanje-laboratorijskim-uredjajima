let db = require('./db');

function getAllUsersDAL(callback) {
    let sql = `SELECT username from User`;

    db.all(sql, [], (err, rows) => {
        if (err) {
            callback(err, null);
        } 
        let users = [];
        rows.forEach((row) => {
            console.log(row.username);
            users.push(row.username);
        });
        
        callback(null, users);
    });
}

function getUserByIdDAL(id, callback) {
    let sql = `SELECT * from User where User.id = ?`;

    db.all(sql, [id], (err, rows) => {
        if (err) {
            callback(err, null);
        } 
        if (! (rows.length > 0)) {
            callback(404, null); // the user not found
        } else {
            let user = rows[0];
            callback(null, user);
        }
    });
}

function getUserByUsernameDAL(username, callback) {
    let sql = `SELECT * from User where User.username = ?`;

    db.all(sql, [username], (err, rows) => {
        if (err) {
            callback(err, null);
        } 
        if (! (rows.length > 0)) {
            callback(404, null); // the user not found
        } else {
            let user = rows[0];
            callback(null, user);
        }
    });
}

function createUserDAL(user, callback) {
    let sql = `INSERT INTO User (username, password) VALUES (?, ?)`;

    db.run(sql, [user.username, user.password], function(err) {
        if (err) {
            console.log(err);
            switch (Number(err.errno)) {
                case 19:
                    err = "Username already exists";
                    break;
                default:
                    break;
            }
            callback(err, null);
        } else {
            console.log(`User with username:${user.username} and password:${user.password} successfully created`);
            exports.getUserByUsernameDAL(user.username, function(err, user) {
                callback(err, user);
            });
        }
    });
}

function updateUserDAL(user, callback) {
    let sql = `UPDATE User SET name = ?, description = ?, text = ? where id = ?`;

    getUserByIdDAL(user.id, function(err, oldUser) {
        if (err) {
            console.log(`Error getting user with id ${user.id}`);
            callback("Couldn't update user", null);
        } else { 
            // update only those values that are changed by checking for null values (|| operator)
            db.run(sql, [user.name || oldUser.name, user.description || oldUser.description, user.text || oldUser.text, user.id], function(err) {
                if (err) {
                    console.log(err);
                    callback("Couldn't update user", null);
                } else {
                    console.log(`Successfully updated user: ${user.id}`);
                    callback(null, user);
                }
            });
        }
    });
}

function deleteUserDAL(id, callback) {
    let sql = `DELETE FROM User where id = ?`;

    db.run(sql, [id], function(err) {
        if (err) {
            console.log(err);
            callback("Couldn't delete user");
        } else {
            console.log("User has been deleted successfully");
            callback(null, `User ${id} has been deleted successfully`);
        }
    });
}

module.exports = {
    createUserDAL,
    getAllUsersDAL,
    getUserByIdDAL,
    getUserByUsernameDAL,
    updateUserDAL,
    deleteUserDAL
}
