const dbConfig = require('../db/dbconfig');
const sqlite3 = require('../node_modules/sqlite3').verbose();

let db = new sqlite3.Database('./db/' + dbConfig.path, (err) => {
    if (err) {
        console.error(err.message);
    } else {
        console.log("Successful connection to DB");
    }
});

module.exports = db;