let db = require('./db');

function getAllDevicesDAL(callback) {
    let sql = `SELECT * from Device`;

    db.all(sql, [], (err, rows) => {
        if (err) {
            callback(err, null);
        } 
        let devices = [];
        rows.forEach((row) => {
            devices.push({
                id: row.id,
                name: row.name,
                IPv4: row.IPv4,
                port: row.port
            });
        });
        
        callback(null, devices);
    });
}

function getDeviceByIdDAL(id, callback) {
    let sql = `SELECT * from Device where id = ?`;

    db.all(sql, [id], (err, rows) => {
        if (err) {
            callback(err, null);
        } 
        if (! (rows.length > 0)) {
            callback(404, null); // the device not found
        } else {
            let device = rows[0];
            callback(null, device);
        }
    });
}

function createDeviceDAL(device, callback) {
    let sql = `INSERT INTO Device (name, IPv4, port) VALUES (?, ?, ?)`;

    db.run(sql, [device.name, device.IPv4, device.port], function(err) {
        if (err) {
            console.log(err);
            callback("Couldn't add device", null);
        } else {
            device.id = this.lastID;
            console.log(`Device with id: ${device} successfully created`);
            callback(null, device);
        }
    });
}

function updateDeviceDAL(device, callback) {
    let sql = `UPDATE Device SET name = ?, IPv4 = ?, port = ? where id = ?`;

    db.run(sql, [device.name, device.IPv4, device.port, device.id], function(err) {
        if (err) {
            console.log(err);
            callback("Couldn't update device", null);
        } else {
            console.log(`Successfully updated device: ${device.id}`);
            getDeviceByIdDAL(device.id, function(err, device) {
                callback(null, device);
            });
        }
    });
}

function deleteDeviceDAL(id, callback) {
    let sql = `DELETE FROM Device where id = ?`;

    db.run(sql, [id], function(err) {
        if (err) {
            console.log(err);
            callback("Couldn't delete device");
        } else {
            console.log("Device has been deleted successfully");
            callback(null, `Device ${id} has been deleted successfully`);
        }
    });
}

module.exports = {
    getAllDevicesDAL,
    getDeviceByIdDAL,
    createDeviceDAL,
    updateDeviceDAL,
    deleteDeviceDAL
}
