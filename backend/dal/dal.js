const users = require('./users');
const devices = require('./devices');
const commands = require('./commands');

module.exports = {
    /* USERS */
    createUserDAL: users.createUserDAL,
    getAllUsersDAL: users.getAllUsersDAL,
    getUserByIdDAL: users.getUserByIdDAL,
    getUserByUsernameDAL: users.getUserByUsernameDAL,
    updateUserDAL: users.updateUserDAL,
    deleteUserDAL: users.deleteUserDAL,
    /* DEVICES */
    createDeviceDAL: devices.createDeviceDAL,
    getAllDevicesDAL: devices.getAllDevicesDAL,
    getDeviceByIdDAL: devices.getDeviceByIdDAL,
    updateDeviceDAL: devices.updateDeviceDAL,
    deleteDeviceDAL: devices.deleteDeviceDAL,
    /* COMMANDS */
    getAllCommandsDAL: commands.getAllCommandsDAL,
    getCommandByIdDAL: commands.getCommandByIdDAL,
    createCommandDAL: commands.createCommandDAL,
    updateCommandDAL: commands.updateCommandDAL,
    deleteCommandDAL: commands.deleteCommandDAL,
}