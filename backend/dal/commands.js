let db = require('./db');

function getAllCommandsDAL(callback) {
    let sql = `SELECT * from Command`;

    db.all(sql, [], (err, rows) => {
        if (err) {
            callback(err, null);
        } 
        let commands = [];
        rows.forEach((row) => {
            commands.push({
                id: row.id,
                name: row.name,
                description: row.description,
                text: row.text
            });
        });
        
        callback(null, commands);
    });
}

function getCommandByIdDAL(id, callback) {
    let sql = `SELECT * from Command where id = ?`;

    db.all(sql, [id], (err, rows) => {
        if (err) {
            callback(err, null);
        } 
        if (! (rows.length > 0)) {
            callback(404, null); // the command not found
        } else {
            let command = rows[0];
            callback(null, command);
        }
    });
}

function createCommandDAL(command, callback) {
    let sql = `INSERT INTO Command (name, description, text) VALUES (?, ?, ?)`;

    db.run(sql, [command.name, command.description, command.text], function(err) {
        if (err) {
            console.log(err);
            callback("Couldn't add command", null);
        } else {
            command.id = this.lastID;
            console.log(`Command with id: ${command} successfully created`);
            callback(null, command);
        }
    });
}

function updateCommandDAL(command, callback) {
    let sql = `UPDATE Command SET name = ?, description = ?, text = ? where id = ?`;

    getCommandByIdDAL(command.id, function(err, oldCommand) {
        if (err) {
            console.log(`Error getting command with id ${command.id}`);
            callback("Couldn't update command", null);
        } else { 
            // update only those values that are changed by checking for null values (|| operator)
            db.run(sql, [command.name || oldCommand.name, command.description || oldCommand.description, command.text || oldCommand.text, command.id], function(err) {
                if (err) {
                    console.log(err);
                    callback("Couldn't update command", null);
                } else {
                    console.log(`Successfully updated command: ${command.id}`);
                    callback(null, command);
                }
            });
        }
    });
}

function deleteCommandDAL(id, callback) {
    let sql = `DELETE FROM Command where id = ?`;

    db.run(sql, [id], function(err) {
        if (err) {
            console.log(err);
            callback("Couldn't delete command");
        } else {
            console.log("Command has been deleted successfully");
            callback(null, `Command ${id} has been deleted successfully`);
        }
    });
}

module.exports = {
    getAllCommandsDAL,
    getCommandByIdDAL,
    createCommandDAL,
    updateCommandDAL,
    deleteCommandDAL
}
