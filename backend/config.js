exports.port = 9000;
exports.secret = 'bcdgwetqurfutiypzzmnefoq';
exports.tokenExpiration = 86400;
exports.SCPITimeout = 5000; // ms

// Rigol DS2302 - Oscilloscope
exports.oscilloscope = {
    ip: '147.91.204.30',
    port: 3021
}

// Rigol DM3068 - Digital Multimeter
exports.multimeter = {
    ip: '147.91.204.30',
    port: 3023
}

// Rigol DP811A - Arbitrary Waveform Generator
exports.WaveformGenerator = {
    ip: '147.91.204.30',
    port: 3025
}

// Rigol DP811A - Power Supply
exports.powersupply = {
    ip: '147.91.204.30',
    port: 3027
}