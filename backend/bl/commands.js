var DAL = require('../dal/dal');
var SCPI = require('./external_communication/scpi_commands')

function getAllCommands(req, res, next) {
    DAL.getAllCommandsDAL(function(err, data) {
        if (err) {
            res.status(500).send("Couldn't get commands");
        } else {
            res.status(200).send(data);
        }
    });
}

function getCommandById(req, res, next) {
    let id = req.params.id;
    if (id == null) {
        res.status(400).send("Bad request: id is missing");
    } else {
        DAL.getCommandByIdDAL(id, function(err, data) {
            if (err) {
                res.status(500).send("Couldn't get command");
            } else {
                res.status(200).send(data);
            }
        });
    }
}

function createCommand(req, res, next) {
    let command = req.body;
    if (!command.name || !command.text) {
        res.status(400).send("{error:'Missing some of the following: name'}");
    } else {
        DAL.createCommandDAL(command, function(err, command) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(200).send(command);
            }
        });
    }
}

function updateCommand(req, res, next) {
    let command = req.body;
    if (!command ||
        !command.id) {
        res.status(400).send("{error: Couldn't update command}");
    } else {
        DAL.updateCommandDAL(command, function(err, command) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(200).send(command);
            }
        });
    }
}

function deleteCommand(req, res, next) {
    let id = req.params.id;
    if (id === null) {
        res.status(400).send("Bad request: id is missing");
    }
    DAL.deleteCommandDAL(id, function(err, data) {
        if (err) {
            res.status(500).send("Couldn't delete command");
        } else {
            res.status(200).send(data);
        }
    });
}

function executeCommand(req, res, next) {
    // console.log(`Port: ${req.body.port}`)
    var port = req.body.port;
    var ip = req.body.ip;
    var command = req.body.command;
    console.log(`port:${port}; ip:${ip}; command:${command}`);
    SCPI.execute(port, ip, command, function(data) {
        // console.log(data);
        res.status(200).send(data);
    });
}

module.exports = {
    getAllCommands,
    getCommandById,
    createCommand,
    updateCommand,
    deleteCommand,
    executeCommand
}