const Netcat = require('node-netcat');
const config = require('../../config');
//var ncClient = Netcat.client(config.powersupply.port, config.powersupply.ip);

function execute(port, address, command, callback) {
    var options = {
        // define a connection timeout (milliseconds)
           timeout: config.SCPITimeout,
        // buffer(default, to receive the original Buffer objects), ascii, hex,utf8, base64
         read_encoding: 'buffer'
    }

    // creation of NEW client every time this function is called
    // reason for new client is because of handler, 'data' event, must be unique for each requests
    var client = Netcat.client(port, address, options);

    client.on('open', function () {
        // console.log('============ NetCat connection opened ============');
        client.send(command + '\n', true);
        if (command && !command.includes("?")) {
            callback('Command sent')
        }
    });
        
    client.on('data', function (data) {
        // DEV log
        console.log(data.toString('ascii'));
        // var str = '';
        // //client.close();
        // var fs = require("fs");

        // // var data = "New File Contents";
        // var i = 0;
        // for (i = 11; i < 1400; i++)
        // {
        //     if (str != '')
        //         str += ','
        //     str += parseFloat((data[i]-127-50) * 0.08)
        // }
        // console.log(str)

        // fs.writeFile("temp.bin", data, (err) => {
        // if (err) console.log(err);
        // console.log("Successfully Written to File.");
        // });
        callback(data);
    });
        
    client.on('error', function (err) {
        console.log(err);
    });
        
    client.on('close', function () {
        // console.log('============ NetCat connection closed ============');
    });

    client.start();
}

module.exports = {
    execute
}