var DAL = require('../dal/dal');

function getAllUsers(req, res, next) {
    DAL.getAllUsersDAL((err, data) => {
        if (err) {
            res.status(400).send(err);
        }
        res.status(200).send(data);
    });
}

function getUserById(req, res, next) {
    let id = req.params.id;
    if (id == null) {
        res.status(400).send("Bad request: id is missing");
    }
    DAL.getUserByIdDAL(id, function(err, data) {
        if (err) {
            res.status(500).send("Couldn't get user");
        } else {
            res.status(200).send(data);
        }
    });
}

function createUser(req, res, next) {
    let user = req.body;
    if (! user.username ||
        ! user.password) {
        res.status(400).send("{error:'Missing username and/or password'}");
    } else {
        DAL.createUserDAL(user, function(err, user) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(200).send(user);
            }
        });
    }
}

function getCurrentUser(req, res, next) {
    req.params.id = req.decoded.id;
    getUserById(req, res, next);
}

module.exports = {
    getAllUsers,
    createUser,
    getUserById,
    getCurrentUser
}