var DAL = require('../dal/dal');

function getAllDevices(req, res, next) {
    DAL.getAllDevicesDAL(function(err, data) {
        if (err) {
            res.status(500).send("Couldn't get devices");
        } else {
            res.status(200).send(data);
        }
    });
}

function getDeviceById(req, res, next) {
    let id = req.params.id;
    if (id == null) {
        res.status(400).send("Bad request: id is missing");
    }
    DAL.getDeviceByIdDAL(id, function(err, data) {
        if (err) {
            res.status(500).send("Couldn't get device");
        } else {
            res.status(200).send(data);
        }
    });
}

function createDevice(req, res, next) {
    let device = req.body;
    if (!device.name ||
        !device.IPv4 ||
        !device.port) {
        res.status(400).send("{error:'Missing some of the following: name, IPv4, port'}");
    } else {
        DAL.createDeviceDAL(device, function(err, device) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(200).send(device);
            }
        });
    }
}

function updateDevice(req, res, next) {
    let device = req.body;
    if (!device ||
        !device.id) {
        res.status(400).send("{error: Couldn't update device}");
    } else {
        DAL.updateDeviceDAL(device, function(err, device) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(200).send(device);
            }
        });
    }
}

function deleteDevice(req, res, next) {
    let id = req.params.id;
    if (id === null) {
        res.status(400).send("Bad request: id is missing");
    }
    DAL.deleteDeviceDAL(id, function(err, data) {
        if (err) {
            res.status(500).send("Couldn't delete device");
        } else {
            res.status(200).send(data);
        }
    });
}

module.exports = {
    getAllDevices,
    getDeviceById,
    createDevice,
    updateDevice,
    deleteDevice
}