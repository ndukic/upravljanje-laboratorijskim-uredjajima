const express = require('express');         
const app = express();                      
const config = require('./config.js');      
const bodyparser = require('body-parser');  
const routes = require('./routes/routes');
const morgan = require('morgan');

/* Add body parser to app to enable parsing the POST body */
app.use(bodyparser.urlencoded({extended: true}));
// app.use(bodyparser.json({limit: '10MB'})); // TODO - Check if limitation is really needed

/* Log everything to dev console */
app.use(morgan('dev'));

/* Serve static files - UI of the application */
app.use(express.static("../public"));

/* Assign routes in one line to keep main file clean */
app.use('/api', routes);

/* Start the server */
app.listen(config.port, function () {
    console.log('listening on *:' + config.port);
});